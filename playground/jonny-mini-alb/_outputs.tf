output "lb_id" {
  description = "ID of Load Balancer created"
  value       = module.alb.lb_id
}
