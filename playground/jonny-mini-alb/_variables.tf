variable "name" {
  type        = string
  description = "Name for application load balancer"
  default     = "Jonny-Mini-ALB"
}
