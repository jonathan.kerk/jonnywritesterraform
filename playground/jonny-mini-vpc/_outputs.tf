output "vpc_id" {
  description = "ID of VPC created"
  value       = module.vpc.vpc_id
}
