variable "bucket_name" {
  type        = string
  description = "Bucket name"
  default     = "jonny-nginx-bucket"
}
